#!/bin/bash

set -e

# make sure the data dir exists
mkdir -p $DATA_PATH
cd $DATA_PATH

java -server $JAVA_OPTS \
  -Dcom.bigdata.rdf.sail.webapp.ConfigParams.propertyFile=${BLAZEGRAPH_PATH}/RWStore.properties \
  -jar ${BLAZEGRAPH_PATH}/blazegraph.jar
