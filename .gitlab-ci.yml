# Use Auto DevOps pipeline
# https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Auto-DevOps.gitlab-ci.yml
include:
  - template: Auto-DevOps.gitlab-ci.yml

variables:
  # disable some auto devops tests
  BROWSER_PERFORMANCE_DISABLED: 1
  CODE_QUALITY_DISABLED: 1
  CONTAINER_SCANNING_DISABLED: 1
  DAST_DISABLED: 1
  LICENSE_SCANNING_DISABLED: 1
  LOAD_PERFORMANCE_DISABLED: 1
  PERFORMANCE_DISABLED: 1
  SECRET_DETECTION_DISABLED: 1
  TEST_DISABLED: 1

  # configure auto devops components
  POSTGRES_ENABLED: "false"
  REVIEW_DISABLED: 1
  STAGING_ENABLED: 1

  # configure application
  K8S_SECRET_JAVA_OPTS: -Xmx4g

  # Agent
  KUBE_INGRESS_BASE_DOMAIN: dev.lincsproject.ca
  KUBE_CONTEXT: "calincs/gitlab-agents:dev-agent"
  KUBE_NAMESPACE: $CI_PROJECT_NAME-$CI_COMMIT_REF_SLUG


lint Helm chart:
  stage: test
  image:
    name: alpine/helm
    entrypoint: ["/bin/sh", "-c"]
  script:
    - helm lint chart

build:
  image: docker
  stage: build
  script:
    - if [ -n "$CI_COMMIT_TAG" ]; then
        export CI_APPLICATION_REPOSITORY=${CI_APPLICATION_REPOSITORY:-$CI_REGISTRY_IMAGE};
        export CI_APPLICATION_TAG=${CI_APPLICATION_TAG:-$CI_COMMIT_TAG};
        image_tagged="$CI_REGISTRY_IMAGE:$CI_COMMIT_TAG";
      else
        export CI_APPLICATION_REPOSITORY=${CI_APPLICATION_REPOSITORY:-$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG};
        export CI_APPLICATION_TAG=${CI_APPLICATION_TAG:-$CI_COMMIT_SHA};
        image_tagged="$CI_APPLICATION_REPOSITORY:$CI_APPLICATION_TAG";
      fi
    - image_latest="$CI_APPLICATION_REPOSITORY:latest"
    - echo "Building image for tag $image_tagged"
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker run --rm --privileged multiarch/qemu-user-static --reset -p yes
    - docker context create myBuilder
    - docker buildx create myBuilder --use
    - docker buildx build --platform linux/amd64 --tag "$image_tagged" --tag "$image_latest" --push .
  rules:
    - if: '$BUILD_DISABLED'
      when: never
    - if: '$CI_COMMIT_TAG || $CI_COMMIT_BRANCH'

# build arm64 image
build_arm64:
  image: docker
  stage: build
  timeout: 200 minutes
  script:
    - if [ -n "$CI_COMMIT_TAG" ]; then
        export CI_APPLICATION_REPOSITORY=${CI_APPLICATION_REPOSITORY:-$CI_REGISTRY_IMAGE};
        export CI_APPLICATION_TAG=${CI_APPLICATION_TAG:-$CI_COMMIT_TAG};
        image_tagged="$CI_REGISTRY_IMAGE:$CI_COMMIT_TAG-arm";
      else
        export CI_APPLICATION_REPOSITORY=${CI_APPLICATION_REPOSITORY:-$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG};
        export CI_APPLICATION_TAG=${CI_APPLICATION_TAG:-$CI_COMMIT_SHA};
        image_tagged="$CI_APPLICATION_REPOSITORY:$CI_APPLICATION_TAG-arm";
      fi
    - echo "Building image for tag $image_tagged"
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker run --rm --privileged multiarch/qemu-user-static --reset -p yes
    - docker context create myBuilder
    - docker buildx create myBuilder --use
    - docker buildx build --platform linux/arm64 --tag "$image_tagged" --push .
  rules:
    - when: manual
  allow_failure: true

# Template to override common deployment parameters
.deployment: &deployment
  before_script:
    - rm -f auth
    - echo "lincs:$(openssl passwd -stdin -apr1 <<< $LINCS_PASSWORD)" >> auth
    - auto-deploy use_kube_context
    - auto-deploy ensure_namespace
    - kubectl -n $KUBE_NAMESPACE create secret generic basic-auth --from-file=auth 
      --save-config --dry-run=client -o yaml | kubectl apply -f -

# override some staging parameters
staging:
  <<: *deployment
  variables:
    KUBE_INGRESS_BASE_DOMAIN: stage.lincsproject.ca
    KUBE_CONTEXT: "calincs/gitlab-agents:stage-agent"
    KUBE_NAMESPACE: $CI_PROJECT_NAME-$CI_PROJECT_ID-staging
    K8S_SECRET_JAVA_OPTS: -Xmx8g
  environment:
    url: http://blazegraph.$KUBE_INGRESS_BASE_DOMAIN


# override some production parameters
production_manual:
  <<: *deployment
  variables:
    KUBE_INGRESS_BASE_DOMAIN: lincsproject.ca
    KUBE_CONTEXT: "calincs/gitlab-agents:prod-agent"
    KUBE_NAMESPACE: $CI_PROJECT_NAME-$CI_PROJECT_ID-production
    K8S_SECRET_JAVA_OPTS: -Xmx12g
  environment:
    url: http://blazegraph.$KUBE_INGRESS_BASE_DOMAIN

# deploy data-review environment in prod
bg-review:
  extends: .auto-deploy
  stage: production
  <<: *deployment
  variables:
    KUBE_INGRESS_BASE_DOMAIN: lincsproject.ca
    KUBE_CONTEXT: "calincs/gitlab-agents:prod-agent"
    KUBE_NAMESPACE: $CI_PROJECT_NAME-$CI_PROJECT_ID-review
    K8S_SECRET_JAVA_OPTS: -Xmx12g
  script:
    - auto-deploy check_kube_domain
    - auto-deploy download_chart
    - auto-deploy use_kube_context || true
    - auto-deploy ensure_namespace
    - auto-deploy create_secret
    - auto-deploy deploy
  environment:
    name: bg-review
    url: https://bg-review.$KUBE_INGRESS_BASE_DOMAIN
  rules:
    - if: '$CI_DEPLOY_FREEZE'
      when: never
    - if: '($CI_KUBERNETES_ACTIVE == null || $CI_KUBERNETES_ACTIVE == "") && ($KUBECONFIG == null || $KUBECONFIG == "")'
      when: never
    - if: '$INCREMENTAL_ROLLOUT_ENABLED'
      when: never
    - if: '$INCREMENTAL_ROLLOUT_MODE'
      when: never
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $STAGING_ENABLED'
      when: manual

# deploy to the mirror site
mirror:
  extends: .auto-deploy
  stage: production
  <<: *deployment
  variables:
    KUBE_INGRESS_BASE_DOMAIN: mirror.lincsproject.ca
    KUBE_CONTEXT: "calincs/gitlab-agents:mirror-agent"
    KUBE_NAMESPACE: $CI_PROJECT_NAME-$CI_PROJECT_ID-mirror
    K8S_SECRET_JAVA_OPTS: -Xmx8g
  script:
    - auto-deploy check_kube_domain
    - auto-deploy download_chart
    - auto-deploy use_kube_context || true
    - auto-deploy ensure_namespace
    - auto-deploy create_secret
    - auto-deploy deploy
  environment:
    name: mirror
    url: https://blazegraph.$KUBE_INGRESS_BASE_DOMAIN
  rules:
    - if: '$CI_DEPLOY_FREEZE'
      when: never
    - if: '($CI_KUBERNETES_ACTIVE == null || $CI_KUBERNETES_ACTIVE == "") && ($KUBECONFIG == null || $KUBECONFIG == "")'
      when: never
    - if: '$INCREMENTAL_ROLLOUT_ENABLED'
      when: never
    - if: '$INCREMENTAL_ROLLOUT_MODE'
      when: never
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $STAGING_ENABLED'
      when: manual

# deploy review environment for mirror
mirror-review:
  extends: .auto-deploy
  stage: production
  <<: *deployment
  variables:
    KUBE_INGRESS_BASE_DOMAIN: mirror.lincsproject.ca
    KUBE_CONTEXT: "calincs/gitlab-agents:mirror-agent"
    KUBE_NAMESPACE: $CI_PROJECT_NAME-$CI_PROJECT_ID-mirror-review
    K8S_SECRET_JAVA_OPTS: -Xmx8g
  script:
    - auto-deploy check_kube_domain
    - auto-deploy download_chart
    - auto-deploy use_kube_context || true
    - auto-deploy ensure_namespace
    - auto-deploy create_secret
    - auto-deploy deploy
  environment:
    name: mirror-review
    url: https://bg-review.$KUBE_INGRESS_BASE_DOMAIN
  rules:
    - if: '$CI_DEPLOY_FREEZE'
      when: never
    - if: '($CI_KUBERNETES_ACTIVE == null || $CI_KUBERNETES_ACTIVE == "") && ($KUBECONFIG == null || $KUBECONFIG == "")'
      when: never
    - if: '$INCREMENTAL_ROLLOUT_ENABLED'
      when: never
    - if: '$INCREMENTAL_ROLLOUT_MODE'
      when: never
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $STAGING_ENABLED'
      when: manual