#!/bin/sh
if [ $# -ne 2 ] ; then
    echo 'Specify filename and graph name as parameters'
    exit 1
fi
curl -D- -H 'Content-Type: text/turtle' --upload-file $1 -X POST \
    "https://blazegraph.lincsproject.ca/blazegraph/namespace/kb/sparql?context-uri=$2"

echo
