FROM openjdk:17-slim-buster

LABEL maintainer="zacanbot@gmail.com"
LABEL name="zacanbot/blazegraph"
LABEL org.opencontainers.image.source https://gitlab.com/calincs/infrastructure/blazegraph
LABEL org.opencontainers.image.title "Blazegraph"
LABEL org.opencontainers.image.description "Ultra-scalable, high-performance database from Blazegraph"
LABEL org.opencontainers.image.version "2.1.6_RC"

ENV BLAZEGRAPH_PATH=/opt/blazegraph \
    DATA_PATH=/mnt/data \
    BLAZEGRAPH_URL=https://github.com/blazegraph/database/releases/download/BLAZEGRAPH_2_1_6_RC/blazegraph.jar \
    JAVA_OPTS="-Xmx4g"

USER root

RUN apt-get update; \
    apt-get install -y wget curl nano htop

WORKDIR $BLAZEGRAPH_PATH

RUN wget $BLAZEGRAPH_URL
COPY RWStore.properties .
COPY entrypoint.sh .
RUN chmod +x entrypoint.sh

CMD ["bash", "-c", "${BLAZEGRAPH_PATH}/entrypoint.sh"]