# Blazegraph

A Blazegraph deployment project based on build 2.1.6_RC

* quad mode
* enabled text index (see [full text search](https://github.com/blazegraph/database/wiki/FullTextSearch))
* enabled geo index (see [geo index](https://github.com/blazegraph/database/wiki/GeoSpatial))

## Run Blazegraph locally

```bash
git clone https://gitlab.com/calincs/infrastructure/blazegraph
cd blazegraph
docker compose up
```

Open the admin UI at [http://localhost:9999](http://localhost:9999)
